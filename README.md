# SECTION B - Lazy Loading To Avoid Pagination (vanilla js)


## Getting started
***

Click Here -> [Demo Link](https://next-growth-labs-tasks.gitlab.io/task2/)

- Implemented all the relevant parameters for Lazy Loading to avoid pagination in vanillas JS

- Using EventListener `scroll` the function is called based on the offsetheight of the document for fetching posts

- A Function `isBottomReached()` is written to check if the user has scrolled to the bottom of the page

- Used https://jsonplaceholder.typicode.com/posts public API

- Intially API is called to fetch 10 posts.

- Every time scroll is at the bottom of the page again API is called to fetch further 10 posts

- `Loading` text is display inbetween API calls

- Hosted the website in the gitlab.io.

- For Continuos Deployement script is written as configaration file. Which automates deployement as soon as the changes are commited.