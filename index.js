
const resultsContainer = document.getElementById('results');
const loadingIndicator = document.getElementById('loading');

let page = 1;
const pageSize = 10; // Number of items to load per page
let isLoading = false;

// Function to fetch data from the API
async function fetchData() {
    try {
        isLoading = true;
        loadingIndicator.style.display = 'block';
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=${pageSize}`);
        const data = await response.json();

        // Render the fetched data
        data.forEach(post => {
            const postElement = document.createElement('div');
            // postElement.classList.add('post');
            postElement.innerHTML = `<h2 class='css-heading-itself'>${post.title}</h2><p>${post.body}</p>`;
            resultsContainer.appendChild(postElement);
        });

        page++;
        isLoading = false;
        loadingIndicator.style.display = 'none';
    } catch (error) {
        console.error('Error fetching data:', error);
    }
}

// Function to check if the user has scrolled to the bottom of the page
function isBottomReached() {
    return (window.innerHeight + window.scrollY) >= document.body.offsetHeight;
}

// Event listener for scrolling
window.addEventListener('scroll', () => {
    if (!isLoading && isBottomReached()) {
        fetchData();
    }
});

// Initial data load
fetchData();
